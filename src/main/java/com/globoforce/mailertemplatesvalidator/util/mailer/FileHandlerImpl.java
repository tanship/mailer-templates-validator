package com.globoforce.mailertemplatesvalidator.util.mailer;

import org.apache.log4j.Logger;

public class FileHandlerImpl {

    private static final String[] globocertTypes = { "5", "6", "105", "106" };

    private static final String[] onlineTypes = { "40", "140" };

    private static final String[] backorderTypes = { "212", "213" };

    private static final String TXT_SUFFIX = ".txt";

    private static final String HTML_SUFFIX = ".html";

    private static Logger log = Logger.getLogger(FileHandlerImpl.class);

    private String templateRoot;

    private String emailType;


    /*protected String determineFilename(EmailData emailData)  throws Exception  {
	String file = null;
	if (templateRoot != null) {
	    String mailType = emailData.getLiveType().getEmailType();
	    if (Arrays.asList(globocertTypes).contains(mailType)) {
		file = determineFileNameGlobocerts(emailData);
	    } else if (Arrays.asList(onlineTypes).contains(mailType) || Arrays.asList(backorderTypes).contains(mailType)) {
		file = determineFileNameTemplateEngDefault(emailData);
	    } else {
		file = determineFileNameOrdinary(emailData);
	    }
	} else {
	    log.error("The template file path for email type " + emailType
		    + " is not listed in the mailer.properties file");
	}
	return file;
    }*/

    /**
     * This method will check if file template for the user exist actually.
     * If not, we will take the English template as filename.
     */
    /*private String determineFileNameTemplateEngDefault(EmailData emailData) throws Exception  {
    	String fileNameResult = null;
	String fileNameRequested = null;
	String fileNameEng = null;
	//Construct file name with requested lang
	StringBuilder fileNameRequestedBuilder = new StringBuilder(templateRoot);
	appendTemplateBaseName(emailData, fileNameRequestedBuilder);
	appendTemplateLang(emailData, fileNameRequestedBuilder);
	fileNameRequested = fileNameRequestedBuilder.toString();

	//Construct file name English
	StringBuilder fileNameEnglishBuilder = new StringBuilder(templateRoot);
	appendTemplateBaseName(emailData, fileNameEnglishBuilder);
	fileNameEng = fileNameEnglishBuilder.toString() + "_eng";

	*//*if (useEnglish(fileNameRequested, emailData)){
	    emailData.setStringValue("lang", "eng");
	    fileNameResult = fileNameEng;
	} else {
	    fileNameResult = fileNameRequested;
	}*//*

	log.debug("Using template: " + fileNameResult);
	return fileNameResult;
    }*/

    /*private String determineFileNameGlobocerts(EmailData emailData) {
	StringBuffer fileName = new StringBuffer(templateRoot);
	if (emailData.containsKey("glbcert_design")) {
	    fileName.append(emailData.getValue("glbcert_design"));
	} else if (emailData.containsKey("email_design")) {
	    fileName.append(emailData.getValue("email_design"));
	    fileName.append("_");
	    fileName.append(emailData.getValue("lang"));
	} else {
	    fileName.append("generic2A_eng");
	}
	return fileName.toString();
    }*/

    /**
     * There would be some email types which won't need email_design & lang to build
     * template filename. Thats because a single template would
     * be used for all clients & all languages.
     * @param emailData
     * @return
     */
   /* protected String determineFileNameOrdinary(EmailData emailData) {
	StringBuilder fileName = new StringBuilder(templateRoot);

	appendTemplateBaseName(emailData, fileName);
	appendTemplateLang(emailData, fileName);

	return fileName.toString();
    }
*/

    /**
     * Append base name
     * Base name consists of email design and template name.
     *
     * email_design_template
     *
     * E.G.: amazon_online
     */
   /* private void appendTemplateBaseName(EmailData emailData, StringBuilder fileName) {
	if (!StringUtils.isEmpty((String) emailData.getValue("email_design"))) {
	    fileName.append(emailData.getValue("email_design"));
	    fileName.append("_");
	}

	if (StringUtils.isEmpty((String) emailData.getValue("template"))) {
	    fileName.append(emailData.getLiveType().getTemplate());
	} else {
	    fileName.append((String) emailData.getValue("template"));
	}
    }*/

    /**
     * Append language.
     * amazon_online => amazon_online_brz
     */
    /*private void appendTemplateLang(EmailData emailData, StringBuilder fileName) {
	if (!StringUtils.isEmpty((String) emailData.getValue("lang"))) {
	    fileName.append("_");
	    fileName.append(emailData.getValue("lang"));
	}
    }*/

   /* private boolean useEnglish(String fileNameRequested, EmailData emailData) throws Exception {
	boolean useEnglish = false;
	VelocityUtils.init();

	boolean existHtmlTemplate = Velocity.resourceExists(fileNameRequested + HTML_SUFFIX);
	boolean existTxtTemplate = Velocity.resourceExists(fileNameRequested + TXT_SUFFIX);

	//Check if the template files exists yet. If some of them do not and they are not english templates,
	//we will retrieve the name of the English template.
	if ((!existHtmlTemplate || !existTxtTemplate ) &&
		!("eng".equalsIgnoreCase(emailData.getStringValue("lang")))){
	    log.error("The template file " + fileNameRequested + HTML_SUFFIX + " exists:" + existHtmlTemplate);
	    log.error("The template file " + fileNameRequested + TXT_SUFFIX + " exists:" + existTxtTemplate);

	    useEnglish = true;
	}
	return useEnglish;
    }*/
}
