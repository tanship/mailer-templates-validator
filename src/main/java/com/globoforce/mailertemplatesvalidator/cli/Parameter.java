package com.globoforce.mailertemplatesvalidator.cli;

/**
 * Predefined parameter names that are allowed for command line interface.
 */
enum Parameter {
    ACTION("-Daction="), ARGUMENTS("-Darguments=");

    private final String prefix;

    private Parameter(String prefix) {
	this.prefix = prefix;
    }

    /**
     * Gets parameter's prefix.
     *
     * @return parameter's prefix
     */
    String getPrefix() {
	return prefix;
    }
}
