package com.globoforce.mailertemplatesvalidator.cli;

import com.globoforce.mailertemplatesvalidator.cli.exception.CliException;
import com.globoforce.mailertemplatesvalidator.exception.MailerTemplatesValidatorServiceException;
import com.globoforce.mailertemplatesvalidator.mgr.MailerTemplatesValidatorManager;
import com.globoforce.mailertemplatesvalidator.util.BasicProfiler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.util.StopWatch;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;

import static java.util.Arrays.asList;

/**
 * Command line runner. Must be created and launched only in <code>Main</code> class.
 */
public class CliRunner {
    private static final int OK_CODE = 0;

    private static final int ERROR_CODE = 1;

    private static final String BACKUP_FLAG_ENABLED_VALUE = "backup";

    private static final Logger LOG = LoggerFactory.getLogger(CliRunner.class);

    private static final DateFormat DF = new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss");

    private ApplicationContext applicationContext;

    public int execute(String... args) {
	StopWatch clock = null;
	BasicProfiler profiler = null;
	final String callName = "CLI Run";

	try {
	    profiler = getBasicProfiler();
	    if (profiler.isTimeCheckEnabled()) {
		clock = profiler.startClock(callName);
	    }

	    ParametersExtractor extractor = new ParametersExtractor(asList(args));
	    Action action = extractor.extractAction();
	    String[] actionArguments = extractor.extractActionArguments();

	    LOG.info("Input parameters parsed: action = {}, action arguments = {}", action, Arrays
			    .toString(actionArguments));

	    if (Action.MAILERTEMPLATESVALIDATOR.equals(action)) {
		mailerTemplatesValidator(actionArguments);
	    }
	    return OK_CODE;
	} catch (Throwable e) {
	    LOG.error("Returning " + ERROR_CODE + " as a result code.", e);
	    System.err.println("Operation failed. Check logfile for details");
	    return ERROR_CODE;
	} finally {
	    if (clock != null) {
		profiler.stopClockAndReport(clock, callName);
	    }
	}
    }

    private void mailerTemplatesValidator(String[] actionArguments)
		    throws MailerTemplatesValidatorServiceException, FileNotFoundException,
		    UnsupportedEncodingException {
	if (actionArguments.length != 2) {
	    throw new CliException(
			    "Search of missing email templates requires two arguments: <pkClient> <lang>");
	}
	int pkClient = Integer.valueOf(actionArguments[0]);
	String lang = actionArguments[1];

	geMailerTemplatesValidatorManager().mailerTemplatesValidator(pkClient, lang);
    }

    MailerTemplatesValidatorManager geMailerTemplatesValidatorManager() {
	return (MailerTemplatesValidatorManager) getApplicationContext().getBean("mailerTemplatesValidatorManager");
    }

    BasicProfiler getBasicProfiler() {
	return (BasicProfiler) getApplicationContext().getBean("mailerTemplatesValidatorServiceBasicProfiler");
    }

    private ApplicationContext getApplicationContext() {
	if (applicationContext == null) {
	    applicationContext = new ClassPathXmlApplicationContext(
			    "classpath:mailer-templates-validator-app-context.xml");
	}
	return applicationContext;
    }

    private void logCliInfo(String message) {
	LOG.info(message);
	System.out.println(message);
    }
}
