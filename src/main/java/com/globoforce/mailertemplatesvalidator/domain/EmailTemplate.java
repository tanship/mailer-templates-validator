package com.globoforce.mailertemplatesvalidator.domain;

public class EmailTemplate implements Comparable<EmailTemplate> {

    int emailType;

    String name;

    String template;

    String emailDesign;

    String fileFilter;

    String emailTemplateDb;

    String filePath;

    public int getEmailType() {
	return emailType;
    }

    public void setEmailType(int emailType) {
	this.emailType = emailType;
    }

    public String getName() {
	return name;
    }

    public void setName(String name) {
	this.name = name;
    }

    public String getTemplate() {
	return template;
    }

    public void setTemplate(String template) {
	this.template = template;
    }

    public EmailTemplate() {
    }

    public EmailTemplate(int emailType, String name, String template) {
	this.emailType = emailType;
	this.name = name;
	this.template = template;
    }

    @Override
    public int compareTo(EmailTemplate o) {
	return ((Integer) this.emailType).compareTo(o.emailType);
    }

    public String getEmailDesign() {
	return emailDesign;
    }

    public void setEmailDesign(String emailDesign) {
	this.emailDesign = emailDesign;
    }

    @Override
    public boolean equals(Object o) {
	if (this == o)
	    return true;
	if (o == null || getClass() != o.getClass())
	    return false;

	EmailTemplate that = (EmailTemplate) o;

	if (emailType != that.emailType)
	    return false;
	if (emailDesign != null ? !emailDesign.equals(that.emailDesign) : that.emailDesign != null)
	    return false;
	if (name != null ? !name.equals(that.name) : that.name != null)
	    return false;
	if (template != null ? !template.equals(that.template) : that.template != null)
	    return false;

	return true;
    }

    @Override
    public int hashCode() {
	int result = emailType;
	result = 31 * result + (name != null ? name.hashCode() : 0);
	result = 31 * result + (template != null ? template.hashCode() : 0);
	result = 31 * result + (emailDesign != null ? emailDesign.hashCode() : 0);
	return result;
    }

    @Override
    public String toString() {
	return "EmailTemplate{" +
			"emailType=" + emailType +
			", name='" + name + '\'' +
			", template='" + template + '\'' +
			", emailDesign='" + emailDesign + '\'' +
			", fileFilter='" + fileFilter + '\'' +
			", emailTemplateDb='" + emailTemplateDb + '\'' +
			", filePath='" + filePath + '\'' +
			'}';
    }

    public String getFileFilter() {
	return fileFilter;
    }

    public void setFileFilter(String fileFilter) {
	this.fileFilter = fileFilter;
    }

    public String getEmailTemplateDb() {
	return emailTemplateDb;
    }

    public void setEmailTemplateDb(String emailTemplateDb) {
	this.emailTemplateDb = emailTemplateDb;
    }

    public String getFilePath() {
	return filePath;
    }

    public void setFilePath(String filePath) {
	this.filePath = filePath;
    }
}
