package com.globoforce.mailertemplatesvalidator.util.clientsite;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailTemplateNameParser {
    private static final Logger LOG = LoggerFactory.getLogger(MailTemplateNameParser.class);

    public static final String CASH_TOKEN = "cash_";

    public static final String THANK_YOU_TOKEN = "thank_you_";

    public static final String YOS_PLATEAU_TOKEN = "plateau_";

    public static final String YOS_TOKEN = "yos_";

    /**
     * Determines if the file path is an email or an award
     *
     * @param file
     * @return true is the mailer indicated this is an award
     */
    public boolean isAward(String file) {
	if (file != null && file.length() > 12) {
	    if (file.indexOf("globocert", 12) == 12) {
		return true;
	    }
	}
	return false;
    }

    /**
     * Given a parameter like ./templates/email/averydennison_authorisation_brz.html
     * or ./templates/email/cash_averydennison_authorisation_brz.html
     * or ./templates/email/thank_you_averydennison_authorisation_brz.html     *
     * return a substring from the start of the averydennison_authorisation_brz
     *
     * @param file the full template name as requested by the mailer. e.g. ./templates/email/cash_averydennison_authorisation_brz.html     *
     * @return the resource name, language and format, e.g. averydennison_authorisation_brz.html
     */
    private String getStringFromStartTemplateResource(String file) {
	String startClientName = file.substring(file.lastIndexOf("/") + 1);
	if (isCashPathAlreadyStripped(startClientName)) {
	    startClientName = startClientName.substring(getIndexStartClientName(startClientName));
	} else if (isThankYouAlreadyStripped(startClientName)) {
	    startClientName = startClientName.substring(getIndexStartClientName(startClientName));
	} else if (isYOSPlateauAlreadyStripped(startClientName)) {
	    startClientName = startClientName.substring(getIndexStartClientName(startClientName));
	}
	return startClientName;
    }

    /**
     * Given a file like ./templates/email/averydennison_authorisation_brz.html
     * find the client name and verify that it is a valid client, e.g. averydennison
     * is a valid client identity
     * @param file for example ./templates/email/averydennison_authorisation_brz.html
     * @param clientProxy a proxy through which we can lookup the client id. Passed as 
     * param for mock test
     * @return name of the client e.g. averydennison
     */
 /*   public String getClientName(String file, ClientProxy clientProxy) {
	if (file.lastIndexOf("/") > 0 && file.indexOf("_") > 0) {
	    String sFromStartClient = getStringFromStartTemplateResource(file);
	    String client = sFromStartClient.substring(0, sFromStartClient.indexOf("_"));
	    if (isClientExists(clientProxy, client)) {
		return client;
	    }
	}
	return "";
    }*/

    /**
     * Given a file like ./templates/email/averydennison_authorisation_brz.html
     * find the language
     *
     * @param file for example ./templates/email/averydennison_authorisation_brz.html
     * @return name the lang of the template e.g. brz
     */
    public String getLang(String file) {
	if (file.lastIndexOf("_") > 0 && file.lastIndexOf(".") > 0) {
	    return file.substring(file.lastIndexOf("_") + 1, file.lastIndexOf("."));
	} else {
	    return "";
	}
    }

    /**
     * Given a file like ./templates/email/averydennison_authorisation_brz.html
     * find the language
     *
     * @param file            for example ./templates/email/averydennison_authorisation_brz.html
     * @param isGenericClient true if the client name is not in the file passed
     * @return name the template name, e.g. authorisation
     */
    public String getTemplateName(String file, boolean isGenericClient) {
	if (isGenericClient) {
	    // in here for files like ./templates/email/cs_mail_resend_item_to_dtm_confirmation_eng.html
	    if (file.lastIndexOf("/") > 0 && file.lastIndexOf("_") > 0) {
		return file.substring(file.lastIndexOf("/") + 1, file.lastIndexOf("_"));
	    }
	} else if (file.indexOf("_") > 0 && file.lastIndexOf("_") > 0) {
	    // in here for files like ./templates/email/averydennison_authorisation_brz.html
	    String sFromStartOfClient = getStringFromStartTemplateResource(file);
	    String templateName = sFromStartOfClient
			    .substring(getIndexEndClientName(sFromStartOfClient) + 1,
					    sFromStartOfClient.lastIndexOf("_"));
	    if (isCash(file)) {
		//add "cash_" prefix for cash awards (bug 14813 fix)
		return CASH_TOKEN + templateName;
	    } else {
		return templateName;
	    }
	}
	return "";
    }

    /**
     * Check to see if the file passed indicates a html file
     *
     * @param file for example ./templates/email/averydennison_authorisation_brz.html
     * @return true if file ends in html, false otherwise
     */
    public boolean isHtml(String file) {
	if (StringUtils.isNotBlank(file) && file.lastIndexOf(".") > 0) {
	    return file.indexOf("html", file.lastIndexOf(".") + 1) != -1;
	}
	return false;
    }

    /**
     * Expects to be passed something like './templates/globocert/cash_averydennison_applause_opna_chn.html'
     *
     * @param file
     * @return returns true if it is a cash award template
     */
    public boolean isCash(String file) {
	if (file.indexOf(CASH_TOKEN, file.lastIndexOf("/") + 1) != -1) {
	    if (!isYOSPlateau(file)) {
		return true;
	    }
	}
	return false;
    }

    /**
     * Checks if the award is a thank you award
     * Expects to be passed something like './templates/globocert/thank_you_averydennison_applause_opna_chn.html'
     *
     * @return returns true if it is a thank you award template
     */
    public boolean isThankYou(String file) {
	if (file.indexOf(THANK_YOU_TOKEN, file.lastIndexOf("/") + 1) != -1) {
	    return true;
	}
	return false;
    }

    /**
     * Checks if the award is a years of service plateau award.
     * Expects to be passed something like './templates/globocert/plateau_averydennison_applause_opna_chn.html'
     *
     * @return returns true if it is a years of service plateau award, false otherwise.
     */
    public boolean isYOSPlateau(String file) {
	if (file.indexOf(YOS_PLATEAU_TOKEN, file.lastIndexOf("/") + 1) != -1) {
	    return true;
	}
	return false;
    }

    /**
     * Checks if the award is a years of service award.
     * Expects to be passed something like './templates/globocert/yos_averydennison_applause_chn.html'
     *
     * @return returns true if it is a years of service award, false otherwise.
     */
    public boolean isYOS(String file) {
	if (file.indexOf(YOS_TOKEN, file.lastIndexOf("/") + 1) != -1) {
	    return true;
	}
	return false;
    }

    /**
     * Determines if specified client exist
     * Retrieve client identity, in case of exception returns false.
     * @param clientProxy
     * @param client
     * @return true if client exist, false if not.
     */
/*    private boolean isClientExists(ClientProxy clientProxy, String client) {
	try {
	    return clientProxy.getClientIdentitiesForClientByIdentifier(client) != null;
	} catch (Exception e) {
	    return false;
	}
    }*/

    /**
     * Expects to be passed something like 'thank_you_averydennison_applause_opna_chn.html'
     *
     * @param file
     * @return returns true if it is a thank you award template
     */
    private boolean isThankYouAlreadyStripped(String file) {
	if (file.startsWith(THANK_YOU_TOKEN)) {
	    return true;
	}
	return false;
    }

    /**
     * Expects to be passed something like 'cash_averydennison_applause_opna_chn.html'
     *
     * @param file
     * @return returns true if it is a cash award template
     */
    private boolean isCashPathAlreadyStripped(String file) {
	if (file.startsWith(CASH_TOKEN)) {
	    return true;
	}
	return false;
    }

    /**
     * Expects to be passed something like './templates/globocert/yos_plateau_averydennison_applause_opna_chn.html'
     *
     * @return returns true if it is a years of service plateau template
     */
    private boolean isYOSPlateauAlreadyStripped(String file) {
	if (file.startsWith(YOS_PLATEAU_TOKEN)) {
	    return true;
	}
	return false;
    }

    /**
     * Given the passed file name, find the index at which the client name starts
     *
     * @param for example ./templates/email/averydennison_authorisation_brz.html or
     *            ./templates/email/cash_averydennison_authorisation_brz.html or
     *            ./templates/globocert/thank_you_averydennison_opna_brz.html
     * @return returns the start index of the client name
     */
    private int getIndexStartClientName(String file) {
	if (isCashPathAlreadyStripped(file)) {
	    return CASH_TOKEN.length();
	} else if (isThankYouAlreadyStripped(file)) {
	    return THANK_YOU_TOKEN.length();
	} else if (isYOSPlateauAlreadyStripped(file)) {
	    return YOS_PLATEAU_TOKEN.length();
	}
	return 0;
    }

    /**
     * Given the passed file name, find the index at which the client name ends
     *
     * @param for example ./templates/email/averydennison_authorisation_brz.html or
     *            ./templates/email/cash_averydennison_authorisation_brz.html or
     *            ./templates/globocert/thank_you_averydennison_opna_brz.html
     * @return returns the end index of the client name
     */
    private int getIndexEndClientName(String file) {
	String working = file;
	if (isCashPathAlreadyStripped(file)) {
	    working = file.substring(CASH_TOKEN.length() + 1);
	} else if (isThankYouAlreadyStripped(file)) {
	    working = file.substring(THANK_YOU_TOKEN.length() + 1);
	} else if (isYOSPlateauAlreadyStripped(file)) {
	    working = file.substring(YOS_PLATEAU_TOKEN.length() + 1);
	}
	return working.indexOf("_");
    }

/*    public String getGenericType(boolean isAward, boolean isCash, boolean isThankYou, boolean isYOSPlateau,
		    boolean isYOS,
		    String templateName, String client, String lang) {
	LOG.info((new StringBuilder("getGenericType: isAward:").append(isAward).append(", isCash:").append(isCash)
			.append(", isThankYou:").append(isThankYou).append(", isYos:").append(isYOS)
			.append(", templateName:").append(templateName).toString()));
	String genericType = "_generic_body"; // normal mail
	if (isAward) {
	    String propertyName = "gMail_" + templateName;
	    if (StringUtils.isBlank(clientPropertyRetriever.getPropertyNonStatic(propertyName, client, "---", "--",
			    true))) {
		if (isCash) {
		    //check if there is no template like "gMail_something" (instead of "gMail_cash_something") for cash award. (bug 14813 fix)
		    String templateNameNoCashPrefix = templateName.startsWith(CASH_TOKEN) ? templateName
				    .substring(CASH_TOKEN.length()) : templateName;
		    propertyName = "gMail_" + templateNameNoCashPrefix;
		    if (StringUtils.isBlank(clientPropertyRetriever.getPropertyNonStatic(propertyName, client, "---",
				    "--", true))) {
			genericType = isYOS ? "_award_generic_yos_cash_body" : "_award_generic_cash_body";
			LOG.info((new StringBuilder("isAward, isCash, property for tempalte is blank ")
					.append(genericType).toString()));
		    } else {
			genericType = "_" + templateNameNoCashPrefix;
			LOG.info((new StringBuilder("templateNameNoCashPrefix ")
					.append(genericType).toString()));
		    }

		} else if (isThankYou) {
		    genericType = isYOS ? "_award_generic_yos_thank_you_body" : "_award_generic_thank_you_body";
		    LOG.info((new StringBuilder("isThankYou ").append(genericType).toString()));
		} else if (isYOSPlateau) {
		    genericType = "_plateau_award_generic_body";
		} else {
		    genericType = isYOS ? "_award_generic_yos_body" : "_award_generic_body";
		    LOG.info((new StringBuilder("non cash award ").append(genericType).toString()));
		}
	    } else {
		genericType = "_" + templateName;
		LOG.info((new StringBuilder("getGenericType: is not an award").append(genericType).toString()));
	    }
	}
	return genericType;
    }*/

}
