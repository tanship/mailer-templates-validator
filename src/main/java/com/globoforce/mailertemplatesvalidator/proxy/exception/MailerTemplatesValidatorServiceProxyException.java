package com.globoforce.mailertemplatesvalidator.proxy.exception;

import com.globoforce.mailertemplatesvalidator.exception.MailerTemplatesValidatorServiceException;

public class MailerTemplatesValidatorServiceProxyException extends MailerTemplatesValidatorServiceException {

    public MailerTemplatesValidatorServiceProxyException(String message, Throwable cause) {
	super(message, cause);
    }
}
