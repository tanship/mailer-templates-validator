package com.globoforce.mailertemplatesvalidator.util;

public enum MailerProperties {

    DBURL("db.url"), DBUSER("db.user"), DBPASSWORD("db.password"), MAILERINSTANCENAME(
		    "mailer_instance_name"), SPECIFICEMAILTYPES("specific_email_types"), EMAILTYPE(
		    "emailtype."), DELIMITER(","), HIGHTVOLUMEEMAILTYPE("high_volume_email_type");

    private String property;

    private MailerProperties(String property) {
	this.property = property;
    }

    public String getProperty() {
	return property;
    }

    @Override
    public String toString() {
	return super.toString();
    }
}
