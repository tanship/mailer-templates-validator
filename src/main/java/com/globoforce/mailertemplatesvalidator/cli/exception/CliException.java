package com.globoforce.mailertemplatesvalidator.cli.exception;

/**
 * The runtime exception that might be only thrown from the command line interface.
 */
public class CliException extends RuntimeException {
    public CliException(String message) {
	super(message);
    }

    public CliException(String message, Throwable cause) {
	super(message, cause);
    }
}
