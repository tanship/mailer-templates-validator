package com.globoforce.mailertemplatesvalidator.cli;

/**
 * Predefined actions that are supported by command line interface.
 */
enum Action {
    MAILERTEMPLATESVALIDATOR("mailertemplatesvalidator");

    private final String textId;

    private Action(String textId) {
	this.textId = textId;
    }

    /**
     * Gets text Id of the action. The text Id of the action is meant to be use in
     * commnad line call of the program.
     *
     * @return text Id of the action
     */
    String getTextId() {
	return textId;
    }
}
