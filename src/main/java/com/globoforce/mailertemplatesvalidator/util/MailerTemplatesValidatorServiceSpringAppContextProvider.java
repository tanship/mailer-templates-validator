package com.globoforce.mailertemplatesvalidator.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public final class MailerTemplatesValidatorServiceSpringAppContextProvider implements ApplicationContextAware {
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
	this.applicationContext = applicationContext;
    }

    public static <T> T getBean(String name) {
	return (T) applicationContext.getBean(name);
    }
}
