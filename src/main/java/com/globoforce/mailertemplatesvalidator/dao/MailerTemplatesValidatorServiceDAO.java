package com.globoforce.mailertemplatesvalidator.dao;

import com.globoforce.mailertemplatesvalidator.domain.EmailTemplate;
import com.globoforce.mailertemplatesvalidator.exception.DAOException;

import java.util.Set;

public interface MailerTemplatesValidatorServiceDAO {

    Set<EmailTemplate> getAvailableEmailTemplates(int pkClient, String expectedMailerInstance) throws DAOException;

    Set<String> getPropEmailTemplates(int pkClient, String lang, String emailTemplate) throws DAOException;

    Set<String> getLargepropEmailTemplates(int pkClient, String lang, String emailTemplate) throws DAOException;

    String getClientName(int pkClient) throws DAOException;

    boolean isDbMailClient(int pkClient) throws DAOException;

}
