package com.globoforce.mailertemplatesvalidator.mgr;

import com.globoforce.mailertemplatesvalidator.dao.MailerTemplatesValidatorServiceDAO;
import com.globoforce.mailertemplatesvalidator.domain.EmailTemplate;
import com.globoforce.mailertemplatesvalidator.exception.DAOException;
import com.globoforce.mailertemplatesvalidator.exception.MailerTemplatesValidatorServiceException;
import com.globoforce.mailertemplatesvalidator.util.GetMailerProperties;
import com.globoforce.mailertemplatesvalidator.util.clientsite.MailTemplateNameParser;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.util.*;

public class MailerTemplatesValidatorManagerImpl implements MailerTemplatesValidatorManager {
    private static final Logger LOG = LoggerFactory.getLogger(MailerTemplatesValidatorManagerImpl.class);

    private MailerTemplatesValidatorServiceDAO mailerTemplatesValidatorServiceDAO;

    private GetMailerProperties mailerProperties;

    private String mailerRootPath;

    String excludeEmailTypes;

    private MailTemplateNameParser mailerTemplateNameParser;

    public boolean getEmailTemplatesFromDir(String mailerTemplatePath, String fileFilter) {

	final String matches = fileFilter + ".(txt|html)";
	ArrayList<String> fileNames = null;

	mailerTemplatePath = mailerRootPath + mailerTemplatePath.substring(1);

	File currentPath = new File(mailerTemplatePath);
	String[] fileList = currentPath.list(new FilenameFilter() {
	    public boolean accept(File dir, String name) {
		return name.matches(matches);
	    }
	});
	if (fileList != null) {
	    fileNames = new ArrayList<String>(Arrays.asList(fileList));
	}

	LOG.info("getEmailTemplatesFromDir: mailerTemplatePath: {} ,fileFilter: {}", currentPath, fileFilter);

	return ((fileNames != null) && (!fileNames.isEmpty())) ? true : false;
    }

    private void writeResultToFile(Set<EmailTemplate> setMissingEmailTemplates, int pkClient, String lang)
		    throws FileNotFoundException, UnsupportedEncodingException {

	setMissingEmailTemplates = new TreeSet(setMissingEmailTemplates);

	Calendar localCalendar = Calendar.getInstance(TimeZone.getDefault());
	Date currentTime = localCalendar.getTime();
	String fileName = pkClient + "_" + lang + "_" + localCalendar.get(Calendar.DATE) + "_" +
			localCalendar.get(Calendar.MONTH) + "_" + localCalendar.get(Calendar.YEAR) + "_" +
			System.currentTimeMillis() + ".txt";

	PrintWriter writer = new PrintWriter(fileName, "UTF-8");

	for (EmailTemplate emailTemplate : setMissingEmailTemplates) {
	    writer.println(emailTemplate);
	}
	writer.close();
	LOG.info("mailer-templates-validator for client {} writing to file {}", pkClient, fileName);
	System.out.format("Mailer-templates-validator for client %s and language %s wrote to file %s", pkClient, lang,
			fileName);
    }

    public Set<String> getEmailTypes(Set<EmailTemplate> setMissingEmailTemplates) {
	Set<String> result = new HashSet<String>();
	Iterator<EmailTemplate> it = setMissingEmailTemplates.iterator();
	while (it.hasNext()) {
	    result.add(String.valueOf(((EmailTemplate) it.next()).getEmailType()));
	}

	return result;
    }

    @Override
    public void mailerTemplatesValidator(int pkClient, String lang)
		    throws MailerTemplatesValidatorServiceException, FileNotFoundException,
		    UnsupportedEncodingException {

	Set<EmailTemplate> setMissingEmailTemplates = Collections.synchronizedSet(new HashSet<EmailTemplate>());

	String clientName;
	String emailTemplate;
	String fileFilter;
	String expectedMailerInstance;
	Map<String, String> pathToTemplates;
	String mailerTemplatePath;

	try {
	    expectedMailerInstance = mailerProperties.getMailerInstance();
	    clientName = mailerTemplatesValidatorServiceDAO.getClientName(pkClient);
	    setMissingEmailTemplates = mailerTemplatesValidatorServiceDAO
			    .getAvailableEmailTemplates(pkClient, expectedMailerInstance);

	    //exclude email types
	    if (!excludeEmailTypes.isEmpty()) {
		excludeEmailTypes(setMissingEmailTemplates, excludeEmailTypes);
	    }

	    pathToTemplates = mailerProperties.getPathToTemplates();

	    validatorEmailTemplatesDir(lang, setMissingEmailTemplates, clientName, pathToTemplates);

	    validatorEmailTemplatesDb(pkClient, lang, setMissingEmailTemplates, clientName, pathToTemplates);

	    LOG.info("mailer-templates-validator for client {} and lang {}", pkClient, lang);

	    if ((setMissingEmailTemplates != null) && (!setMissingEmailTemplates.isEmpty())) {
		writeResultToFile(setMissingEmailTemplates, pkClient, lang);
		LOG.info("setMissingEmailTemplates [{}] was written to file",
				setMissingEmailTemplates.size());
	    }

	} catch (DAOException e) {
	    LOG.error("error when mailer-templates-validator for client {}", pkClient);
	    throw new MailerTemplatesValidatorServiceException(
			    "Error when mailer-templates-validator for client " + pkClient, e);
	} catch (IOException e) {
	    LOG.error("Error: ", e);
	}

    }

    private void validatorEmailTemplatesDb(int pkClient, String lang, Set<EmailTemplate> setMissingEmailTemplates,
		    String clientName, Map<String, String> pathToTemplates) throws DAOException {
	String emailTemplate, emailTemplateDb;
	if (mailerTemplatesValidatorServiceDAO.isDbMailClient(pkClient)) {

	    if (!setMissingEmailTemplates.isEmpty()) {

		Iterator<EmailTemplate> it = setMissingEmailTemplates.iterator();
		while (it.hasNext()) {
		    EmailTemplate availableEmailTemplate = it.next();
		    emailTemplate = availableEmailTemplate.getTemplate();
		    String fileName = clientName + "_" + appendTemplateBaseName(availableEmailTemplate) + "_" + lang;
		    String file = availableEmailTemplate.getFilePath() + fileName;

		    LOG.info("mailer-template-validator in MS_PROPS table, file: {}", file);

		    boolean isYOS = mailerTemplateNameParser.isYOS(file);
		    file = isYOS ? file.replace("/" + MailTemplateNameParser.YOS_TOKEN, "/") : file;

		    String genericType = this.getGenericType(mailerTemplateNameParser.isAward(file),
				    mailerTemplateNameParser.isCash(file),
				    mailerTemplateNameParser.isThankYou(file),
				    mailerTemplateNameParser.isYOSPlateau(file),
				    mailerTemplateNameParser.isYOS(file), emailTemplate, clientName,
				    lang, pkClient);

		    emailTemplateDb = getEmailTempateDBSearch(emailTemplate,
				    genericType, mailerTemplateNameParser.isAward(file));

		    availableEmailTemplate.setEmailTemplateDb(emailTemplateDb);

		    LOG.info("mailer-template-validator in MS_PROPS table, emailTemplateDb: {}", emailTemplateDb);

		    Set<String> propEmailTemplates = mailerTemplatesValidatorServiceDAO
				    .getPropEmailTemplates(pkClient, lang, emailTemplateDb);

		    if ((propEmailTemplates != null) && (!propEmailTemplates.isEmpty())) {
			LOG.info("FOUND: {} from availableEmailTemplates was found in MS_PROPS table",
					availableEmailTemplate.toString());
			it.remove();
		    }
		}
	    }
	    if (!setMissingEmailTemplates.isEmpty()) {
		Iterator<EmailTemplate> it = setMissingEmailTemplates.iterator();
		while (it.hasNext()) {
		    EmailTemplate availableEmailTemplate = it.next();
		    emailTemplateDb = availableEmailTemplate.getEmailTemplateDb();

		    LOG.info("mailer-template-validator in GG_MS_LARGE_PROPS table, emailTemplateDb: {}",
				    emailTemplateDb);

		    Set<String> largePropmailTemplates = mailerTemplatesValidatorServiceDAO
				    .getLargepropEmailTemplates(pkClient, lang, emailTemplateDb);

		    if ((largePropmailTemplates != null) && (!largePropmailTemplates.isEmpty())) {
			LOG.info("FOUND: {} from availableEmailTemplates was found in GG_MS_LARGE_PROPS table",
					availableEmailTemplate.toString());
			it.remove();
		    }
		}
	    }
	} else {
	    LOG.info("skip step to find of email templates in DB");
	}
    }

    private String getEmailTempateDBSearch(String emailTemplate,
		    String genericType, boolean isAward) {

	String emailTempateDBSearch;
	if (isAward) {
	    emailTempateDBSearch = "gMail" + genericType;
	} else {
	    emailTempateDBSearch = "eMail_" + emailTemplate + "_body";
	}
	return emailTempateDBSearch;
    }

    private void validatorEmailTemplatesDir(String lang, Set<EmailTemplate> setMissingEmailTemplates, String clientName,
		    Map<String, String> pathToTemplates) {
	String fileFilter;
	String mailerTemplatePath;
	int emailType;

	/*if mailer config file doesn't contain emailtype.* properties
	 then skip step to find of email templates in file system*/

	if ((!setMissingEmailTemplates.isEmpty()) && (pathToTemplates != null) && (!pathToTemplates.isEmpty())) {
	    Iterator<EmailTemplate> iter = setMissingEmailTemplates.iterator();
	    while (iter.hasNext()) {
		EmailTemplate availableEmailTemplate = iter.next();
		emailType = availableEmailTemplate.getEmailType();
		fileFilter = clientName + "_" + appendTemplateBaseName(availableEmailTemplate) + "_" + lang;
		availableEmailTemplate.setFileFilter(fileFilter);

		mailerTemplatePath = pathToTemplates.get(String.valueOf(emailType));
		availableEmailTemplate.setFilePath(mailerTemplatePath);

		if (mailerTemplatePath == null) {
		    LOG.info("mismatching: path for email type {} not found in email property file ",
				    availableEmailTemplate.getEmailType());

		    iter.remove();
		    continue;
		}

		if ((mailerTemplatePath != null) &&
				(this.getEmailTemplatesFromDir(mailerTemplatePath, fileFilter))) {
		    LOG.info("FOUND: {} from availableEmailTemplates was found in mailer-template directory {} with fileFilter {}",
				    availableEmailTemplate.toString(),
				    mailerTemplatePath, fileFilter);
		    iter.remove();
		}
	    }
	} else {
	    LOG.info("skip step to find of email templates in file system perhaps mailer config " +
			    "file doesn't contain emailtype.* properties or change exclude-email-types property");
	}
    }

    private String appendTemplateBaseName(EmailTemplate availableEmailTemplate) {
	// add according to type

	String result;
	result = (!StringUtils.isEmpty(availableEmailTemplate.getEmailDesign()) ?
			(availableEmailTemplate.getEmailDesign() + '_') : "") + availableEmailTemplate.getTemplate();
	return result;
    }

    public MailerTemplatesValidatorServiceDAO getMailerTemplatesValidatorServiceDAO() {
	return mailerTemplatesValidatorServiceDAO;
    }

    public void setMailerTemplatesValidatorServiceDAO(
		    MailerTemplatesValidatorServiceDAO mailerTemplatesValidatorServiceDAO) {
	this.mailerTemplatesValidatorServiceDAO = mailerTemplatesValidatorServiceDAO;
    }

    public GetMailerProperties getMailerProperties() {
	return mailerProperties;
    }

    public void setMailerProperties(GetMailerProperties mailerProperties) {
	this.mailerProperties = mailerProperties;
    }

    public String getMailerRootPath() {
	return mailerRootPath;
    }

    public void setMailerRootPath(String mailerRootPath) {
	this.mailerRootPath = mailerRootPath;
    }

    public String getExcludeEmailTypes() {
	return excludeEmailTypes;
    }

    public void setExcludeEmailTypes(String excludeEmailTypes) {
	this.excludeEmailTypes = excludeEmailTypes;
    }

    private void excludeEmailTypes(Set<EmailTemplate> setEmailTemplates, String excludeEmailTypes) {
	Iterator<EmailTemplate> it = setEmailTemplates.iterator();
	while (it.hasNext()) {
	    EmailTemplate emailTemplate = it.next();

	    if (excludeEmailTypes.contains(String.valueOf(emailTemplate.getEmailType()))) {
		it.remove();
		LOG.info("excludeEmailTypes: {}", emailTemplate.toString());
	    }
	}

    }

    public void setMailerTemplateNameParser(MailTemplateNameParser mailerTemplateNameParser) {
	this.mailerTemplateNameParser = mailerTemplateNameParser;
    }

    public MailTemplateNameParser getMailerTemplateNameParser() {
	return mailerTemplateNameParser;
    }

    private String getGenericType(boolean isAward, boolean isCash, boolean isThankYou, boolean isYOSPlateau,
		    boolean isYOS,
		    String templateName, String client, String lang, int pkClient) throws DAOException {

	final String CASH_TOKEN = "cash_";

	LOG.info((new StringBuilder("getGenericType: isAward:").append(isAward).append(", isCash:").append(isCash)
			.append(", isThankYou:").append(isThankYou).append(", isYos:").append(isYOS)
			.append(", templateName:").append(templateName).toString()));
	String genericType = "_generic_body"; // normal mail
	if (isAward) {
	    String propertyName = "gMail_" + templateName;
	    if (StringUtils.isBlank(String.valueOf(mailerTemplatesValidatorServiceDAO
			    .getLargepropEmailTemplates(pkClient, lang,
					    propertyName)))) {
		if (isCash) {
		    //check if there is no template like "gMail_something" (instead of "gMail_cash_something") for cash award. (bug 14813 fix)
		    String templateNameNoCashPrefix = templateName.startsWith(CASH_TOKEN) ? templateName
				    .substring(CASH_TOKEN.length()) : templateName;
		    propertyName = "gMail_" + templateNameNoCashPrefix;
		    if (StringUtils.isBlank(String.valueOf(mailerTemplatesValidatorServiceDAO
				    .getLargepropEmailTemplates(pkClient, lang,
						    propertyName)))) {
			genericType = isYOS ? "_award_generic_yos_cash_body" : "_award_generic_cash_body";
			LOG.info((new StringBuilder("isAward, isCash, property for tempalte is blank ")
					.append(genericType).toString()));
		    } else {
			genericType = "_" + templateNameNoCashPrefix;
			LOG.info((new StringBuilder("templateNameNoCashPrefix ")
					.append(genericType).toString()));
		    }

		} else if (isThankYou) {
		    genericType = isYOS ? "_award_generic_yos_thank_you_body" : "_award_generic_thank_you_body";
		    LOG.info((new StringBuilder("isThankYou ").append(genericType).toString()));
		} else if (isYOSPlateau) {
		    genericType = "_plateau_award_generic_body";
		} else {
		    genericType = isYOS ? "_award_generic_yos_body" : "_award_generic_body";
		    LOG.info((new StringBuilder("non cash award ").append(genericType).toString()));
		}
	    } else {
		genericType = "_" + templateName;
		LOG.info((new StringBuilder("getGenericType: is not an award").append(genericType).toString()));
	    }
	}
	return genericType;
    }

}
