package com.globoforce.mailertemplatesvalidator;

import com.globoforce.mailertemplatesvalidator.exception.MailerTemplatesValidatorServiceException;
import com.globoforce.mailertemplatesvalidator.mgr.MailerTemplatesValidatorManagerImpl;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * Entry point for command line mailer-templates-validator tool.
 */
public final class MainTest {

    public static void main(String[] str)
		    throws FileNotFoundException, UnsupportedEncodingException,
		    MailerTemplatesValidatorServiceException {

	ConfigurableApplicationContext context = new ClassPathXmlApplicationContext(
			"mailer-templates-validator-app-context.xml");

	MailerTemplatesValidatorManagerImpl mailerTemplatesValidatorManagerImplTest = context
			.getBean(MailerTemplatesValidatorManagerImpl.class);
	mailerTemplatesValidatorManagerImplTest.mailerTemplatesValidator(33, "eng");
    }

}
