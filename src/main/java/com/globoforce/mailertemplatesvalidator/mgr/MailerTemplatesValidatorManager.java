package com.globoforce.mailertemplatesvalidator.mgr;

import com.globoforce.mailertemplatesvalidator.exception.MailerTemplatesValidatorServiceException;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

public interface MailerTemplatesValidatorManager {

    void mailerTemplatesValidator(int pkClient, String lang)
		    throws MailerTemplatesValidatorServiceException, FileNotFoundException,
		    UnsupportedEncodingException;
}
