package com.globoforce.mailertemplatesvalidator;

import com.globoforce.mailertemplatesvalidator.cli.CliRunner;

/**
 * Entry point for command line mailer-templates-validator tool.
 */
public final class Main {
    private static CliRunner cliRunner = new CliRunner();

    public static void main(String... args) {

	System.exit(cliRunner.execute(args));
    }
}
