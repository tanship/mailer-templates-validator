package com.globoforce.mailertemplatesvalidator.dao;

import com.globoforce.mailertemplatesvalidator.domain.EmailTemplate;
import com.globoforce.mailertemplatesvalidator.exception.DAOException;
import com.globoforce.mailertemplatesvalidator.util.GetMailerProperties;
import com.globoforce.mailertemplatesvalidator.util.MailerProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class MailerTemplatesValidatorServiceDAOImpl extends JdbcDaoSupport
		implements MailerTemplatesValidatorServiceDAO {

    private static final Logger LOG = LoggerFactory.getLogger(MailerTemplatesValidatorServiceDAOImpl.class);

    public MailerTemplatesValidatorServiceDAOImpl() {

    }

    public MailerTemplatesValidatorServiceDAOImpl(GetMailerProperties mailerProperties) throws IOException {

	Map<String, String> map = mailerProperties.getDbMailerPropValues();

	DriverManagerDataSource dms = new org.springframework.jdbc.datasource.DriverManagerDataSource(map.get(
			MailerProperties.DBURL.getProperty()),
			map.get(MailerProperties.DBUSER.getProperty()), map.get(
			MailerProperties.DBPASSWORD.getProperty()));

	dms.setDriverClassName("oracle.jdbc.driver.OracleDriver");
	this.setDataSource(dms);
    }

    @Override
    public Set<EmailTemplate> getAvailableEmailTemplates(int pkClient, String expectedMailerInstance)
		    throws DAOException {
	LOG.info("SQL: {}, with params: pkClient={} expectedMailerInstance={}",
			SQLStatements.SQL_GET_AVAILABE_TEMPLATES, pkClient,
			expectedMailerInstance);
	Set<EmailTemplate> setAvailableEmailTemplates = new HashSet<EmailTemplate>();

	SqlRowSet rows = getJdbcTemplate().queryForRowSet(SQLStatements.SQL_GET_AVAILABE_TEMPLATES,
			new Object[] { pkClient, expectedMailerInstance });
	while (rows.next()) {
	    EmailTemplate emailTemaplate = new EmailTemplate();
	    emailTemaplate.setEmailType(rows.getInt("EMAIL_TYPE"));
	    emailTemaplate.setName(rows.getString("NAME"));
	    emailTemaplate.setTemplate(rows.getString("TEMPLATE"));

	    //add EMAIL_DESIGN property
	    emailTemaplate.setEmailDesign(getEmailDesignByClient(emailTemaplate.getEmailType()));

	    setAvailableEmailTemplates.add(emailTemaplate);
	}

	return setAvailableEmailTemplates;
    }

    public String getStoreCodeFromId(long clientID) {
	if (clientID < 10) {
	    return "INTCOR000" + clientID;
	} else if (clientID < 100) {
	    return "INTCOR00" + clientID;
	} else if (clientID < 1000) {
	    return "INTCOR0" + clientID;
	}
	return "INTCOR" + clientID;
    }

    @Override
    public Set<String> getPropEmailTemplates(int pkOwner, String lang, String emailTemplate) throws DAOException {
	LOG.info("SQL: {}, with params: lang= {} pkOwner={} emailTemplate={}", SQLStatements.SQL_GET_PROP_TEMPLATES,
			lang, pkOwner, emailTemplate);
	Set<String> setPropEmailTemplates = new HashSet<String>();

	SqlRowSet rows = getJdbcTemplate().queryForRowSet(SQLStatements.SQL_GET_PROP_TEMPLATES,
			new Object[] { lang, pkOwner, emailTemplate });
	while (rows.next()) {
	    setPropEmailTemplates.add(rows.getString("NAME"));
	}
	return setPropEmailTemplates;
    }

    @Override
    public Set<String> getLargepropEmailTemplates(int pkOwner, String lang, String emailTemplate) throws DAOException {
	LOG.info("SQL: {}, with params: lang= {} pkOwner={} emailTemplate={}",
			SQLStatements.SQL_GET_LARGEPROP_TEMPLATES, lang, pkOwner,
			emailTemplate);
	Set<String> setLargepropEmailTemplates = new HashSet<String>();

	SqlRowSet rows = getJdbcTemplate().queryForRowSet(SQLStatements.SQL_GET_LARGEPROP_TEMPLATES,
			new Object[] { lang, pkOwner, emailTemplate });
	while (rows.next()) {
	    setLargepropEmailTemplates.add(rows.getString("NAME"));
	}
	return setLargepropEmailTemplates;

    }

    @Override
    public String getClientName(int pkClient) throws DAOException {
	try {
	    LOG.info("SQL {}, with params: {}", SQLStatements.SQL_GET_CLIENTS, pkClient);
	    return (String) getJdbcTemplate().queryForObject(SQLStatements.SQL_GET_CLIENTS,
			    new Object[] { pkClient },
			    new ParameterizedRowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				    return rs.getString(1);
				}
			    });
	} catch (EmptyResultDataAccessException e) {
	    throw new DAOException("No client Name found for pkClient " + pkClient, e);
	} catch (IncorrectResultSizeDataAccessException e) {
	    throw new DAOException("More than one client Name found for pkClient " + pkClient, e);
	} catch (DataAccessException e) {
	    throw new DAOException("Error during fetching client Name for pkClient " + pkClient, e);
	}
    }

    @Override
    public boolean isDbMailClient(int pkClient) throws DAOException {
	LOG.info("SQL {}, with params: {}", SQLStatements.SQL_GET_ISDBMAILCLIENT, pkClient);

	int result = getJdbcTemplate().queryForInt(SQLStatements.SQL_GET_ISDBMAILCLIENT,
			new Object[] { pkClient });
	return (result > 0 ? true : false);
    }

    private String getEmailDesignByClient(int pkClient) throws DAOException {
	String stoStoreCode = getStoreCodeFromId(pkClient);
	String result = null;
	try {
	    LOG.info("SQL {}, with params: {}", SQLStatements.SQL_GET_EMAIL_DESIGN, stoStoreCode);
	    result = (String) getJdbcTemplate().queryForObject(SQLStatements.SQL_GET_EMAIL_DESIGN,
			    new Object[] { stoStoreCode },
			    new ParameterizedRowMapper<String>() {
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
				    return rs.getString(1);
				}
			    });

	} catch (EmptyResultDataAccessException e) {
	}
	return result;
    }
}

