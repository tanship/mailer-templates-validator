package com.globoforce.mailertemplatesvalidator.util;

import java.io.*;
import java.util.*;

public class GetMailerProperties {

    private String propFileName;

    private Properties prop;

    public GetMailerProperties() {

    }

    public GetMailerProperties(String propFileName) throws IOException {
	this.propFileName = propFileName;
	prop = new Properties();
	File f = new File(propFileName);
	InputStream inputStream = new FileInputStream(f);
	prop.load(inputStream);
	if (inputStream == null) {
	    throw new FileNotFoundException("property file '" + propFileName + "' not found");
	}
    }

    public Map<String, String> getDbMailerPropValues() throws IOException {
	Map<String, String> result = new HashMap<String, String>();

	result.put(MailerProperties.DBURL.getProperty(), prop.getProperty(MailerProperties.DBURL.getProperty()));
	result.put(MailerProperties.DBUSER.getProperty(), prop.getProperty(MailerProperties.DBUSER.getProperty()));
	result.put(MailerProperties.DBPASSWORD.getProperty(),
			prop.getProperty(MailerProperties.DBPASSWORD.getProperty()));
	return result;
    }

    public String getEmailTypes() throws IOException {
	String result = prop.getProperty(MailerProperties.SPECIFICEMAILTYPES.getProperty());
	return (result != null ? result : "");
    }

    public String gethHighVolumeEmailTypes() throws IOException {
	String result = prop.getProperty(MailerProperties.HIGHTVOLUMEEMAILTYPE.getProperty());
	return (result != null ? result : "");
    }

    public Map<String, String> getPathToTemplates() throws IOException {
	Map<String, String> result = new HashMap<String, String>();
	Set<String> emailTypesSet = new HashSet<String>(
			java.util.Arrays.asList(this.getEmailTypes().split(MailerProperties.DELIMITER.getProperty())));

	emailTypesSet.addAll(java.util.Arrays.asList(
			this.gethHighVolumeEmailTypes().split(MailerProperties.DELIMITER.getProperty())));

	String emailTypeSearch;

	for (String emailtype : emailTypesSet) {
	    emailTypeSearch = MailerProperties.EMAILTYPE.getProperty() + emailtype;
	    String emailTypeProperty = prop.getProperty(emailTypeSearch);
	    if (emailTypeProperty != null) {
		result.put(emailtype, emailTypeProperty);
	    }

	}
	return result;
    }

    public String getMailerInstance() throws IOException {
	return prop.getProperty(MailerProperties.MAILERINSTANCENAME.getProperty());
    }

    public String getPropFileName() {
	return propFileName;
    }

    public void setPropFileName(String propFileName) {
	this.propFileName = propFileName;
    }

}
