package com.globoforce.mailertemplatesvalidator.util;

import org.aspectj.lang.ProceedingJoinPoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StopWatch;

import java.util.concurrent.TimeUnit;

public class BasicProfiler {
    private static final Logger LOG = LoggerFactory.getLogger(BasicProfiler.class);

    private static final String WATCH_NAME_PATTERN = "Profiling %s";

    private boolean timeCheckEnabled;

    public void setTimeCheckEnabled(boolean timeCheckEnabled) {
	this.timeCheckEnabled = timeCheckEnabled;
    }

    public boolean isTimeCheckEnabled() {
	return timeCheckEnabled;
    }

    public Object profile(ProceedingJoinPoint call) throws Throwable {
	if (timeCheckEnabled) {
	    StopWatch clock = null;

	    try {
		clock = startClock(call.toShortString());
		return call.proceed();
	    } finally {
		stopClockAndReport(clock, call.toShortString());
	    }
	} else {
	    return call.proceed();
	}
    }

    public StopWatch startClock(String callName) {
	StopWatch clock = new StopWatch(String.format(WATCH_NAME_PATTERN, callName));
	clock.start(callName);
	return clock;
    }

    public void stopClockAndReport(StopWatch clock, String callName) {
	clock.stop();
	long ts = clock.getTotalTimeMillis();
	LOG.info("Execution time of '{}': {}ms, {}m:{}s:{}ms",
			callName,
			ts,
			TimeUnit.MILLISECONDS.toMinutes(ts),
			TimeUnit.MILLISECONDS.toSeconds(ts) - TimeUnit.MINUTES.toSeconds(
					TimeUnit.MILLISECONDS.toMinutes(ts)),
			ts - TimeUnit.SECONDS.toMillis(TimeUnit.MILLISECONDS.toSeconds(ts)));
    }

}
