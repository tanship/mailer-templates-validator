package com.globoforce.mailertemplatesvalidator.cli;

import com.globoforce.mailertemplatesvalidator.cli.exception.CliException;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

/**
 * Extracts the parameters from the input arguments provided.
 */
class ParametersExtractor {
    private static final Logger LOG = LoggerFactory.getLogger(ParametersExtractor.class);

    private final List<String> args;

    /**
     * Creates the extractor and takes the program's input arguments to parse.
     *
     * @param args arguments to parse
     */
    ParametersExtractor(List<String> args) {
	this.args = args;
    }

    /**
     * Extracts and returns the action from the input parameters.
     * <p/>
     * Unchecked exception might be thrown, if there are errors during extraction.
     *
     * @return the predefined action
     */
    Action extractAction() {
	try {
	    String argValue = extractParameterValue(Parameter.ACTION.getPrefix());
	    for (Action action : EnumSet.allOf(Action.class)) {
		if (StringUtils.equals(action.getTextId(), argValue)) {
		    LOG.info("action {} extracted from input args", action);
		    return action;
		}
	    }
	    LOG.error("no action extracted. Action {} is not supported", argValue);
	    throw new CliException("Unknown action found for action parameter: " + argValue);
	} catch (IllegalArgumentException e) {
	    throw new CliException("No action defined", e);
	}

    }

    /**
     * Extracts and returns the action arguments from the input parameters.
     * <p/>
     * The arguments must be comma separated values
     *
     * @return arrays of the parsed action arguments
     */
    String[] extractActionArguments() {
	try {
	    String argValue = extractParameterValue(Parameter.ARGUMENTS.getPrefix());
	    List<String> actionArguments = new ArrayList<String>();
	    for (String actionArgument : StringUtils.split(argValue, ',')) {
		actionArguments.add(StringUtils.trimToEmpty(actionArgument));
	    }
	    LOG.info("action arguments {} extracted from input args", actionArguments);
	    return actionArguments.toArray(new String[actionArguments.size()]);
	} catch (IllegalArgumentException e) {
	    throw new CliException("No action arguments defined", e);
	}
    }

    private String extractParameterValue(final String prefix) {
	for (String arg : args) {
	    if (arg.startsWith(prefix)) {
		String argValue = StringUtils.trimToEmpty(StringUtils.substringAfter(arg, prefix));
		if (StringUtils.isEmpty(argValue)) {
		    LOG.error("the value for prefix {} is empty", prefix);
		    throw new IllegalArgumentException("The value for prefix " + prefix + " is empty");
		}
		return argValue;
	    }
	}
	LOG.error("parameter for prefix {} is required, but not found", prefix);
	throw new IllegalArgumentException("Parameter for prefix " + prefix + " is required, but not found");
    }
}
