package com.globoforce.mailertemplatesvalidator.proxy;

import com.globoforce.mailertemplatesvalidator.mgr.MailerTemplatesValidatorManager;
import com.globoforce.mailertemplatesvalidator.proxy.exception.MailerTemplatesValidatorServiceProxyException;
import com.globoforce.mailertemplatesvalidator.util.MailerTemplatesValidatorServiceSpringAppContextProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MailerTemplatesValidatorServiceProxy {

    private static final Logger LOG = LoggerFactory.getLogger(MailerTemplatesValidatorServiceProxy.class);

    private static MailerTemplatesValidatorServiceProxy instance;

    private final MailerTemplatesValidatorManager mailerTemplatesValidatorManager;

    private MailerTemplatesValidatorServiceProxy() throws MailerTemplatesValidatorServiceProxyException {
	try {
	    mailerTemplatesValidatorManager = MailerTemplatesValidatorServiceSpringAppContextProvider
			    .getBean("mailerTemplatesValidatorManager");
	} catch (Exception e) {
	    LOG.error("error in <init>", e);
	    throw new MailerTemplatesValidatorServiceProxyException("error in <init>", e);
	}
    }

    public static MailerTemplatesValidatorServiceProxy getInstance()
		    throws MailerTemplatesValidatorServiceProxyException {
	if (instance == null) {
	    instance = new MailerTemplatesValidatorServiceProxy();
	}
	return instance;
    }

}
