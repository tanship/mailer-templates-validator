package com.globoforce.mailertemplatesvalidator.exception;

public class MailerTemplatesValidatorServiceException extends Exception {
    public MailerTemplatesValidatorServiceException(String message, Throwable cause) {
	super(message, cause);
    }

    public MailerTemplatesValidatorServiceException(String message) {
	super(message);
    }

    public MailerTemplatesValidatorServiceException(Throwable cause) {
	super(cause);
    }
}
