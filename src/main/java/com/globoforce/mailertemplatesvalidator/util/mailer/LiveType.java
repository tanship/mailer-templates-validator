package com.globoforce.mailertemplatesvalidator.util.mailer;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Arrays;
import java.util.List;

public class LiveType {

    private String emailType;

    private String name;

    private String template;

    private String preprocess;

    private String postprocess;

    private String test;

    private String mailerName;
    
    //list of SCT email type ids
    static final public List<Integer> contestToolEmailTypeIds = Arrays.asList(new Integer[] {1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009, 1010});

    @Deprecated
    public LiveType(String emailType, String name, String template,
                    String preprocess, String postprocess, String test) {
	this(emailType, name, template, preprocess, postprocess, null, test);
    }

    public LiveType(String emailType, String name, String template,
                    String preprocess, String postprocess, String mailerName, String test) {
        this.emailType = emailType;
        this.name = name;
        this.template = template;
        this.preprocess = preprocess;
        this.postprocess = postprocess;
        this.mailerName = mailerName;
        this.test = test;
    }

    /**
     * Gets the emailType.
     *
     * @return Returns a String
     */
    public String getEmailType() {
        return emailType;
    }
    
    /** 
     * added for easy comparison.
     */
    public Integer getType() {
        return Integer.decode(emailType.trim());
    }

    /**
     * Gets the name.
     *
     * @return Returns a String
     */
    public String getName() {
        return name;
    }

    /**
     * Gets the template.
     *
     * @return Returns a String
     */
    public String getTemplate() {
        return template;
    }

    /**
     * Gets the preprocess.
     *
     * @return Returns a String
     */
    public String getPreprocess() {
        return preprocess;
    }

    /**
     * Gets the postprocess.
     *
     * @return Returns a String
     */
    public String getPostprocess() {
        return postprocess;
    }

    /**
     * Gets the test.
     *
     * @return Returns a String
     */
    public String getTest() {
        return test;
    }
    
    public boolean isTest() {	
	return  getTest().equals("1");
    }

    public boolean equals(Object obj) {
       if(obj instanceof LiveType) {
           LiveType othType = (LiveType)obj;
           if(this.getEmailType().equals(othType.getEmailType())
               && this.getName().equals(othType.getName()))
               return true;
       }
       return false;
    }

    public int hashCode() {
        int result = 17;
        result = 37*result + emailType.hashCode();
        result = 37*result + name.hashCode();
        return result;
    }
    public boolean isContestType() {
	int liveType = Integer.decode(getEmailType());
	if (contestToolEmailTypeIds.contains(liveType)) {
	    return true;
	}
	return false;
    }
    
    /**
     * Checks whether the preprocess sql is normal or extended type
     * @return boolean true if preprocess is extended, false otherwise. 
     */
    public boolean isExtendedSQL() {
	if (preprocess == null) {
	    return false;
	}
	int index = preprocess.indexOf("{", 0);
	return (index >= 0);
    }

    public String getMailerName() {
        return mailerName;
    }

    public void setMailerName(String mailerName) {
        this.mailerName = mailerName;
    }

    @Override
    public String toString() {
	return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}