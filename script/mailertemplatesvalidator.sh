#!/bin/bash

java -Xms128m -Xmx256m -cp "lib/*:." com.globoforce.mailertemplatesvalidator.Main -Daction=mailertemplatesvalidator -Darguments="$1,$2"