package com.globoforce.mailertemplatesvalidator.dao;

public final class SQLStatements {

    public static final String SQL_GET_AVAILABE_TEMPLATES =
		    "SELECT EMAIL_TYPE, NAME, TEMPLATE FROM GG_MAIL_CONFIG " +
				    "WHERE EMAIL_TYPE NOT IN (SELECT EMAIL_TYPE FROM GG_MAIL_DISABLE WHERE FK_CLIENT= ?) AND " +
				    "GG_MAIL_CONFIG.LIVE=1 AND " +
				    "EXPECTED_MAILER_INSTANCE = ? ORDER BY EMAIL_TYPE";

    public static final String SQL_GET_PROP_TEMPLATES =
		    "SELECT NAME FROM GG_MS_PROPS WHERE (LANG = '---' OR LANG= ?) AND PK_OWNER=? AND NAME = ?";

    public static final String SQL_GET_LARGEPROP_TEMPLATES =
		    "SELECT NAME FROM GG_MS_LARGE_PROPS WHERE (LANG = '---' OR LANG= ?) AND PK_OWNER=? AND NAME = ?";

    public static final String SQL_GET_CLIENTS =
		    "SELECT NAME FROM GG_MS_CLIENTS WHERE PK_CLIENT=? ";

    public static final String SQL_GET_ISDBMAILCLIENT =
		    "SELECT COUNT(*) FROM GG_MS_CLIENTS c, GG_MS_PROPS p WHERE c.pk_client=p.pk_owner " +
				    "AND p.NAME='g_mailer_db_template' AND p.OWNER_ID='1' AND p.VALUE='true' and pk_client=?";

    public static final String SQL_GET_EMAIL_DESIGN = "SELECT EMAIL_DESIGN FROM gg_stores WHERE STO_STORE_CODE = ?";

}
